package com.shifumix;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.remote.UnreachableBrowserException;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    static List<String> lg=new ArrayList<String>();
    static long startLog=System.currentTimeMillis();

    public static void lg(String s){
        Long delay=(Long) (System.currentTimeMillis()-startLog)/(1000);
        lg.add(delay+":"+s);
        System.out.println(delay+":"+s);
    }

    public Integer num(String s){
        Integer rc=0;
        for(String ss:lg)
            if(ss.toLowerCase().indexOf(s.toLowerCase())>-1)rc++;
        return rc;
    }


    public static void main(String[] args) throws UnreachableBrowserException,StaleElementReferenceException,MalformedURLException, SearchException,ClassCastException {



        long delay = 1000 * 60 * 20000;
        if (args.length > 4) delay = Long.parseLong(args[4]) * 1000 * 60;
        long dtEnd=System.currentTimeMillis()+delay;

        long start = System.currentTimeMillis();
        System.out.println("argument <email> <password> ");
        System.out.println("Start");

        Shifumix a=new Shifumix(args[2]);
        a.sleep(0.5);
        a.refresh();

        List<String> events= Arrays.asList(
                args[0].split("@")[0]+" party;on va trop s'amuser;14 rue de milan paris",
                args[0].split("@")[0]+" night;on est pas bien ici;12 rue martel paris",
                args[0].split("@")[0]+" night;on va trop s'amuser;86 rue du faubourg St denis paris",
                "Conference SFR;Presentation de la stratégie;Campus SFR, Saint-Denis"
        );


        if (args[3].indexOf("raz")!=-1)a.raz();
        if (args[3].indexOf("scenario1")!=-1)a.scenario1(args[0], args[1]);

        if (args[3].indexOf("scenario_tennis")!=-1)a.scenario_tennis();

        if (args[3].indexOf("patterns")!=-1){
            a.openClient(args[0],args[1]);
            //a.sleep(1.0);
            //while(a.selEvent(null))a.sleep(2.0);
            if(!a.selEvent(null))
                a.makeEvent("Martel's night", "Uniquement pour hébergé les patterns", "12 rue Martel, paris", null, null, null);

            while(!a.selEvent(null))a.sleep(1.0);
            for(int i=0;i<a.patterns.size();i++){
                a.sleep(1.0);
                a.addBetPattern(a.patterns.get(i));
            }
            a.quitEvent();
        }

        if (args[3].indexOf("fakeagent")!=-1){
            String username=args[0];
            if(username.indexOf("random")>-1)
                username=username.replace("random",String.valueOf(Math.round(Math.random()*9000)+1000));

            a.openClient(username,args[1]);
            a.sleep(1.0);
            if(a.inEvent())a.quitEvent();
            if (args[3].indexOf("addevent")!=-1)a.makeEvent("Martel's night",
                    "on va trop s'amuser sous la pluie", "12, rue martel paris",null,null,"http://www.chocobea.com/wp-content/uploads/2010/02/Cocotte-12.jpg");

            //a.createEvent(a.tirage(events));

            Main.lg("Boucle d'attente pendant "+(System.currentTimeMillis()-dtEnd)/60000);
            int i=0;
            while(System.currentTimeMillis()<dtEnd){
                if(a.selEvent(null)){
                    a.fakeUser(dtEnd,0);
                }else{
                    i++;
                    if(i>8){
                        a.createEvent(a.tirage(events));
                        i=0;
                    }
                }
                a.sleep(2.0);
            }
            a.quitEvent();
            a.logOff();
        }

        System.out.println("Temps de traitement "+(delay/(1000*60)));
    }


}
