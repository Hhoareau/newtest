package com.shifumix;

import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.UnreachableBrowserException;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

/**
 * Created by u016272 on 04/10/2016.
 */
public class Shifumix extends Applicatif {

    String domain="http://localhost:8080";

    List<String> titres= Arrays.asList("pixies","nirvana","pj harvey","daughter",
            "bon iver","coldplay","cure","daft punk","carl cox","new order","radiohead",
            "pantha du prince","laurent garnier","prince","mickael jackson","depeche mode",
            "pavement","sonic youth","nas","dr dre","dj sneak","deerhunter","joy division",
            "jesus and mary chain","baths","elastica","bjork","tom york");

    List<String> emails = Arrays.asList("paul.dudule@gmail.com","herve.hoareau@sfr.com",
            "hoareau.herve@sfr.fr","roger.legumes@gmail.com",
            "hhoareau@gmail.com","sophie.dudule@gmail.com",
            "shifumixweb@gmail.com","admin@shifumix.com");

    List<String> bets = Arrays.asList(
            "Cuisine;Y a quoi dans la cocotte ?;De la viande vu qu'il sait faire que ça hoareau;Du poisson car il s'est souvenu qu'il vivait avec une végétarienne;Ni l'un ni l'autre, que du vin;5;6",
            "Soirée;Combien de bouteilles va-t-on boire ?;Aucune, que de l'eau;moins de 3;entre 3 et 5;entre 6 et 8;entre 9 et 11;plus de 12;8;3",
            "Tennis,Sport;Pouille/Murray - qui gagne ?;Pouille en 2 sets;Pouille en 3 sets;Murray en 2 sets;Murray en 3 sets;4;5",
            "Tennis,Sport;Nishikory/Tsonga- qui gagne ?;Nishikory en 2 sets;Nishikory en 3 sets;Tsonga en 2 sets;Tsonga en 3 sets;5;5",
            "Tennis,Sport;Berdych/Simon - qui gagne ?;Berdych en 2 sets;Berdych en 3 sets;Simon en 2 sets;Simon en 3 sets;8;5",
            "Sport,foot,psg;PSG gagne à la fin du match;1-0;2-0;3-1;5;6",
            "Politique,débat,usa;Avant la fin du débat Hillary;quitte le plateau;colle une baffe à Trump;Fond en larme;4;6",
            "Sport,course,athlé;Ussan gagne le 100 metres en moins de;9.1 secondes;9.3 secondes;9.6 secondes;5;10",
            "sport,vélo,tour de france;Quelle sera la nationnalité du vainqueur de l'étape;;Française;Belge;Hollandaise;Allemande;Américaine;5;8",
            "Business,entreprise communication;A votre avis, la fibre sera déployé dans :;Plus de 1M de foyers;Plus de 2M de foyers;Plus 3M de foyer;5;5",
            "Politique;Seront présents au second tour;LePen - Sarko;Holland - Sarko;LePen - Mélanchon;LePen - Juppé;Juppé - Mélanchon;5;3",
            "Connaissance géographie;Toulouse à;- de 200 000 habitants;de 200 à 400 000 habitants;+ de 400 00 habitants;5;10"
    );

    String tags="rock inde music politique societe bio equitable geek troquet programmation " +
                "troc entreprise marx philo enfants electro pop punk nerd java javascript architecture " +
                "culture peinture hegel jouer pari vélo voiture";

    List<String> surveys= Arrays.asList(
            "Divers;Selon vous, le nom de l'appli ça devrait être quoi ?;Shifumix;Chiffon mix;FouChiMix;On s'en branle ... du nom;On s'en branle du nom ... et du reste;20;2",
            "Politique,societe;Pour la primaire de la droite, vous y allez comment ?;Avec votre chemisette préférée;Avec un pull croisé sur les épaules;Avec une cagoulle;Avec votre serre tête; En jupe culotte vichy;Avec vos dockside;30;3",
            "Politique;Primaire de droite;Juppé;Sarko;Lemaire;NKM;Macron;10;3",
            "Application,ergonomie,simplicite,android,iphone;Comment améliorer Shifumix;Simplifier simplifier simplifier...;ajouter de nouvelles fonctionnalité;pouvoir jouer de l'argent;faire connaitre le site;10;3",
            "Business;Pour une stratégie d'entreprise que faut-il privilégier ? ...;les clients;les salariés;les actionnaires;les banquiers;5;2",
            "Politique,élections;Le candidat de la gauche au second tour sera;Bayrou;Hamon;Hollande;Royal;Mélanchon;10;3",
            "Entreprise;Diriez vous que la présentation est;Super;Interessante mais mal animée;Inintéréssante;Vous n'auriez jamais dû venir;5;2",
            "Entreprise;Pensez vous que la stratégie présentée soit la bonne;oui;non;ne sais pas;10;1",
            "Entreprise;Go projet ?;Oui;Oui avec réserve;Non;Ne sait pas;5;1",
            "Entreprise;Quelle perception ont les gens de notre marque;Très bonne;Bonne;Moyenne;Mauvaise;Très mauvaise;il ne l'a connaisse même pas;10;3",
            "Transport;Que faudrait-il pour améliorer cette station;Plus de boutique;Plus d'agent;Moins de publicité;Refaire les peintures;10;2",
            "Transport;Trouver que les trains sont;Suffisament à l'heure;Jamais à l'heure;Pas d'avis;10;1",
            "Loisir;Quel est votre cocktail préféré ever;Mojito;Caipi;Bloody Mary;Gin Fizz;10;1"
    );

    List<String> loteries= Arrays.asList(
            "Un joli vélo,Presque neuf,rnd:10,rnd:50,rnd:5,rnd:10",
            "une voiture,Voiture neuve n'ayant jamais roulée,0,rnd:20,50,rnd:10"
    );


    List<String> messages= Arrays.asList(
            "trop bien ici","j'ai rien compris à l'histoire là","c'est par ou la sortie","y a pas trop de beaux gosses aujourd'hui","faut il arrêter les jets d'eau quand il pleut",
            "y aura-t-il de la neige à noel","bla bla bla bla, c'est toujours la même chose","Vous pensez finir vers qu'elle heure ?");


            List<String> photos= Arrays.asList(
            "https://ericheymans.com/wp-content/uploads/2012/08/photographie-de-paysage-11.jpg","http://i24.servimg.com/u/f24/11/24/10/54/1111.jpg",
                    "http://maximumwallhd.com/wp-content/uploads/2015/07/fonds-ecran-paysage-de-reve-81.jpg","https://s-media-cache-ak0.pinimg.com/originals/95/fa/3b/95fa3bffe048f2782ca7eb76650164da.jpg",
                    "http://resize2-parismatch.ladmedia.fr/img/var/news/storage/images/paris-match/brouillons/madonna-pousse-un-coup-de-gueule-sur-instagram-c-est-mon-parking-936185/13315113-1-fre-FR/Madonna-pousse-un-coup-de-gueule-sur-Instagram-C-est-mon-parking.jpg",
                    "https://media.pitchfork.com/photos/592c563deb335119a49f0ca2/master/w_790/25efaaff.jpg","http://www.maisons-moyse.fr/typo3temp/_processed_/csm_slide3_6a822d9089.jpg",
                    "https://prodwebassets.s3.amazonaws.com/boats/4681976/4681976_20140410082304491_1_XLARGE.jpg","https://static-cdn.arte.tv/cdnp-concert/sites/default/files/events/backgrounds/time-warp-event-neu.jpg",
                    "http://images.sudouest.fr/2017/06/10/593cff3066a4bd110e1d1215/default/1000/ambiance-survoltee.jpg"
    );


    List<String> patterns = Arrays.asList(
            "Foot,score,final;Pronostic #Equipe1 vs #Equipe2 ?;#Equipe1 1 - 0 #Equipe2;#Equipe1 0 - 1 #Equipe2;#Equipe1 1 - 1 #Equipe2;#Equipe1 2 - 0 #Equipe2;#Equipe1 0 - 2 #Equipe2;#Equipe1 0 - 0 #Equipe2;5;Score final de match de 0 à 2 buts",
            "Foot,score,final;Pronostic #Equipe1 vs #Equipe2 ?;#Equipe1 gagne avec 1 but d'avance;#Equipe1 gagne avec 2 buts d'avance;#Equipe2 gagne avec 1 but d'avance;#Equipe2 gagne avec 2 buts d'avance;5;Score final de match avec 1 et 2 buts d'avance",
            "Tennis;#Joueur1 vs #Joueur2;#Joueur1 gagne en 2 sets;#Joueur2 gagne en 2 sets;#Joueur1 gagne en 3 sets;#Joueur2 gagne en 3 sets;6;Score final de tennis",
            "Basket;#Equipe1 vs #Equipe2;#Equipe1 gagne avec PLUS de 20 pts d'avance;#Equipe1 gagne avec MOINS de 20 pts d'avance;#Equipe2 gagne avec PLUS de 20 pts d'avance;#Equipe2 gagne avec MOINS de 20 pts d'avance;6;Score final de basket",
            "Elections,2eme tour,#Candidat1 vs #Candidat2;#Candidat1 gagne avec plus de 52% des voix;#Candidat1 gagne avec moins de 52% des voix;#Candidat1 gagne avec plus de 52% des voix;#Candidat1 gagne avec moins de 52% des voix;6;Second tour d'élection",
            "Elections,Primaires;Qui sera présent au second tour;#Candidat1 présent au second tour;#Candidat2 présent au second tour;#Candidat3 présent au second tour;#Candidat4 présent au second tour;#Candidat5 présent au second tour;6;Primaires de parti",
            "Elections,premier tour;#Candidat1 présent au second tour;#Candidat2 présent au second tour;#Candidat3 présent au second tour;#Candidat4 présent au second tour;#Candidat5 présent au second tour;6;Premier tour d'élection",
            "Basket;Combien #NomJoueur va-t'il réussir de panier à 3 points;#NomJoueur mettra moins de 3;Plus de 3;Plus de 6;Plus de 10;6;Nombre de paniers à 3 pts", "Soirée,Amis,picole;Combien va t'on boire de bouteille ce soir ?;Moins de #Pallier1;Entre #Pallier1 et #Pallier2;Entre #Pallier2 et #Pallier3;Plus de #Pallier3;5;Nombre de bouteille sur 3 palliers"
    );


    public Shifumix() throws MalformedURLException {
        super();
    }

    public Shifumix(String domain) throws MalformedURLException {
        super();
        this.setDomain(domain);
        webTo(domain,2.0);
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String getContent() {
     return null;
    }

    @Override
    public void closeClient() {
    }

    @Override
    public void openClient(String user, String password) throws SearchException {

        Main.lg("OpenClient");

        if(password.equals("null")){
            sleep(3.0);
            click("btnShowEmailLogin");
            sleep(2.0);
            send(user,getElt("name","txtEmail","input"));
            click("btnValideUser");
            send("1234",getElt("txtPassword"));
            sleep(1.0);
            click("btnLogin");
            sleep(3.0);
        } else {
            webTo("https://www.facebook.com/login.php?email=" + user, 1.0);

            if(web.getCurrentUrl().indexOf("https://www.facebook.com/login.php")!=0){
                WebElement elt=getElt("action","https://www.facebook.com/logout.php","form");
                if(elt!=null)click(elt);
                webTo("https://www.facebook.com/login.php?email=" + user,1.0);
            }
            sleep(1.0);
            WebElement zone=getElt("class","login_form_container","div");
            send(password, getElt("pass",zone));
            click(getElt("loginbutton",zone));
            webTo(domain + "/?notuto=true",6.0);
            click("btnFacebookLogin");
            sleep(6.0);
        }
    }

    public void remove_tuto(){
        WebElement div=null;
        try{
            div=web.findElement(By.id("div_tuto"));
        }catch (NoSuchElementException e){}
        if(div!=null)click(div);
    }

    /**
     *
     * @param name
     * @param teaser
     * @param adresse
     * @param dt
     * @param password
     * @param flyer
     * @return
     */
    public Boolean  makeEvent(String name,String teaser,String adresse,String dt,String password,String flyer){
        Main.lg("MakeEvent");

        Long date= System.currentTimeMillis();
        try {
            if(dt!=null)date = new SimpleDateFormat("dd/MM/yy hh:mm", Locale.FRANCE).parse(dt).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(!click("btnAddEvent")){
            web.navigate().refresh();
            return false;
        }


        sleep(1.0);
        send(name, getElt("txtTitle"));
        send(teaser,getElt("txtDescription"));
        click("btnStep1");sleep(3.0);

        send(adresse, getElt("txtAddress"));
        valide("txtAddress");
        sleep(2.0);

        click("btnStep2");sleep(3.0);

        //Horaire
        send(String.valueOf(tirage(2)),getElt("txtStartIn"));
        send(String.valueOf(tirage(3)+1),getElt("txtDuration"));
        click("btnStep3");

        sleep(1.0);

        click("btnInstagram");
        sleep(1.0);
        this.enterPopup("BigParty");
        sleep(2.0);

        click(getElts("flyerImages"));
        sleep(1.0);
        click("btnStep4");

        send(password, getElt("password"));
        click("chkVisibility");
        click("btnSave");
        return true;
    }

    public String addSong(String title,Integer index){
        if(web.getCurrentUrl().indexOf("/tabs/")==-1)return("");
        Main.lg("AddSong:" + title);

        if(!tab(1))return "";
        click("btnAddSong2");
        sleep(1.0);

        send(title,getElt("txtSearch"));
        click("btnSearch");
        sleep(8.0);

        List<WebElement> rc=getElts("author","span");
        if(rc.size()==0)return web.getCurrentUrl();
        Main.lg("Resultats : "+rc.size());

        if(index>=rc.size())index=0;
        Boolean b=click(rc.get(index));
        sleep(2.0);

        if(!b)web.navigate().back();

        return web.getCurrentUrl();
    }

    public String raz(){
        Main.lg("Raz");
        webTo(domain + "/admin/admin.html", 2.0);
        click(getElt("RAZ"));
        sleep(1.0);
        return null;
    }

    public Boolean selEvent(String password) throws StaleElementReferenceException,UnhandledAlertException {
        if(web.getCurrentUrl().indexOf("/tabs/")>0)return true;

        start();
        List<WebElement> rc=new ArrayList<WebElement>();

        if(rc.size()==0){
            click("btnZoomOn");
            sleep(1.0);
        }
        rc=getElts("flyers", "div");
        /*
        if(rc.size()==0){
            rc=getElts("name","flyers","img");
            if(rc.size()>1)rc.remove(0);
            Boolean b=click(rc);
            sleep(2.0);
            return b;
        }*/

        if(rc.size()==0)return false;

        click(rc);
        boolean b=click("btnJoin");

        if(password!=null)send(password,getElt("txtPassword"));

        return b;
    }

    public String delEvent(Integer index) {
        Main.lg("#delEvent");
        getElts("btnDelete").get(index).click();
        sleep(2.0);
        return "ok";
    }


    public void makeBetSurvey(String title, String opts,int delay,String tags){
        sleep(2.0);
        send(title,getElt("txtTitle"));
        for(String s:opts.split(";")){
            send(s,getElt("add_opt"));
            valide("add_opt");
            sleep(0.01);
        }
        if(delay==0)delay=1;

        send(String.valueOf(tirage(5)),getElt("txtStartBet"));
        send(String.valueOf(delay), getElt("txtEndBet"));
        send(String.valueOf(tirage(5)+2),getElt("txtCagnotte"));
        send(String.valueOf(tirage(5)),getElt("txtMinAmount"));

        /*
        if(getElt("txtAddTag")!=null){
            for(String tag:tags.split(",")){
                send(tag,getElt("txtAddTag"));
                valide("txtAddTag");
            }
        }
        */

    }

    public Boolean addBet(String tag,String title, String opts, int delay,int cagnotte) {
        Main.lg("#addBet:"+title);
        if(!tab(3))return false;
        scrollTop();
        if(click(getElt("btnNewBet"))){
            makeBetSurvey(title,opts,delay,tag);
            if(getElt("txtCagnotte")!=null)send(String.valueOf(tirage(cagnotte)),getElt("txtCagnotte"));
            click("btnSave");
            sleep(3.0);
            return true;
        }
        return false;
    }

    public void addBetPattern(String tags,String title, String opts,int delay,String patternTitle) throws SearchException {
        Main.lg("#addBet:"+title);
        sleep(2.0);
        if(!tab(3))return;
        if(click(getElt("btnNewBet"))){
            makeBetSurvey(title,opts,delay,tags);
            click("btnSave");

            send(patternTitle,getElt("input_popup"));
            WebElement popup=getElt("class","popup","div");
            click(getElts("button", popup).get(1));

            sleep(3.0);
        }
    }



    public void addSurvey(String tag,String title, String opts, int delay,int nbVotes) {
        if(web.getCurrentUrl().indexOf("/tabs/")==-1)return;
        Main.lg("#addSurvey:"+title);
        if(!tab(3))return;
        scrollTop();
        if(click(getElt("btnNewSurvey"))){
            makeBetSurvey(title,opts,delay,tag);
            send(nbVotes + "", getElt("txtNbVotes"));
            click("btnSave");
            sleep(2.0);
        }
    }

    public void shareBets(String email,Integer index) throws StaleElementReferenceException {
        if(web.getCurrentUrl().indexOf("/tabs/")==-1)return;
        Main.lg("#shareBets to "+email);
        tab(3);
        List<WebElement> elts=getElts("btnShare","button");
        boolean rc=false;
        if(elts.size()>0){
            if(index==null)
                rc=click(elts);
            else
                rc=click(elts.get(index));

            if(rc){
                sleep(1.0);
                sendInvite(email);
            }
        }
    }

    public void shareBets(String email) throws StaleElementReferenceException {
        shareBets(email,null);
    }

    public void scrollTop(){
        ((JavascriptExecutor)web).executeScript("window.scrollTo(0,0);");
    }

    public void misebet(int somme) throws SearchException,StaleElementReferenceException,ClassCastException {
        if(web.getCurrentUrl().indexOf("/tabs/")==-1)return;
        Main.lg("#miseBet:"+somme);
        if(!tab(3))return;
        scrollTop();
        List<WebElement> elts=getElts("btnBet","button");
        if(elts.size()>0){
            click(elts);
            send(somme+"",getElt("input_popup"));sleep(0.5);
            WebElement popup=getElt("class","popup","div");sleep(0.5);
            click(getElts("button", popup).get(1));
        }
    }

    public void voteSurvey() throws SearchException,StaleElementReferenceException {
        if(web.getCurrentUrl().indexOf("/tabs/")==-1)return;
        Main.lg("#vote");
        if(!tab(3))return;
        scrollTop();
        click(getElts("btnVote","button"));
    }


    private boolean click(List<WebElement> elts) throws StaleElementReferenceException {
        if(elts.size()==0)return false;
        int index=(int) Math.round(Math.random()*(elts.size()-1)-0.5);
        return click(elts.get(index));
    }

    public void notePlayer(){
        Main.lg("#notePlayer");
        tab(3);
        String s="btnVotePos";
        if (tirage(1) == 0) s ="btnVoteNeg";
        click(getElts(s, "button"));
    }

    public void quitEvent() {
        Main.lg("#quitEvent:Sortie de l'event");
        if(web.getCurrentUrl().indexOf("/tabs/")>0){
            tab(6);
            remove_tuto();
            click("btnQuitEvent");
            sleep(8.0);
        }
    }

    public void logOff() {
        Main.lg("#logOff:deconnexion de facebook");
        if(tab(6))return;
        click("btnLogoff");
        sleep(3.0);
    }

    public void validateBets() throws SearchException {
        Main.lg("#validateBet");
        if(!tab(3))return;
        List<WebElement> elts=getElts("btnValidate","button");
        if(elts.size()>0){
            WebElement elt=getElts("option_bet","label").get(1);
            click(elt);
            click(elts.get(0));

            WebElement popup=getElt("class","popup-buttons","div");
            if(popup!=null)click(getElts("button", popup).get(1));
        }
    }

    public void removeFriends(){
        Main.lg("#RemoveFriends");
        if(!tab(5))return;
        click(getElts("btnDelete"));
    }

    public void showCharts(int index){
        if(!tab(4))return;
        click(getElt(tirage("btnCredits,btnScore")));
    }


    public void voteSong() {
        Main.lg("#voteSong");
        if(!tab(1))return;
        for(int i=0;i<4;i++){
            sleep(3.0);
            click(getElts("btnLike", "button"));
            click(getElts("btnDisLike", "button"));
        }
    }

    public void playMusic(String username,String password) {
        Main.lg("#playMusic");
        if(!tab(1))return;
        click(getElts("name", "public_option", "ion-item").get(0));
        sleep(10.0);

        web.switchTo().frame(0);

        send(username, getElt("login_mail"));
        send(password,getElt("login_password"));
        click("login_form_submit");

        web.switchTo().window("My Profil");
    }

    public void playPhoto() {
        if(!tab(5))return;
        sleep(2.0);
        remove_tuto();

        click(getElts("public_option").get(1));
        sleep(2.0);
        ArrayList<String> tabs = new ArrayList<String> (web.getWindowHandles());
        web.switchTo().window(tabs.get(0));
    }


    private String tirage(String s) {
        return tirage(Arrays.asList(s.split(",")));
    }

    public static int tirage(int max){
        return (int) Math.round(Math.random()*max);
    }

    public String tirage(List<String> emails) {
        return emails.get(tirage(emails.size()-1));
    }

    public void fakeUser(long dtEnd,Integer waitDelay) throws UnreachableBrowserException,SearchException,ClassCastException {
        Main.lg("Evenement ouvert. Simulation d'agent");
        this.addPhoto(tirage(photos));
        //this.addBet(bets);
        //this.addSurvey(surveys);
        //this.addSong(tirage(titres), tirage(5));
        //this.addSong(tirage(titres), tirage(5));
        this.addSong(tirage(titres), tirage(5));

        Main.lg("Boucle de comportement.");
        while(dtEnd-System.currentTimeMillis()>0){
            try{
                if(this.isClosed()){
                    webTo(domain+"/#/selEvent",5.0);
                    break;
                }
                if(tirage(20)==0)this.addBet(bets);
                if(tirage(20)==0)this.addSurvey(surveys);
                if(tirage(20)==0)this.addLoterie(loteries);
                if(tirage(10)==0)this.postMessage(tirage(messages));
                if(tirage(10)==0)this.votePhoto();
                if(tirage(15)==0)this.addSong(tirage(titres), tirage(10));
                if(tirage(40)==0)this.addBetPattern(tirage(patterns));
                if(tirage(3)==0)this.voteSong();
                if(tirage(6)==0)this.voteSurvey();
                if(tirage(3)==0)this.misebet(tirage(5) + 1);
                if(tirage(3)==0)this.validateBets();
                //if(tirage(30)==0)this.addTag(tags);
                if(tirage(3)==0)this.notePlayer();
                if(tirage(20)==0)this.changeUserPicture();
                if(tirage(20)==0)this.openWidget();
                if(tirage(10)==0)this.rotatePhoto();
                if(tirage(20)==0)this.removeMessage();
                if(tirage(20)==0)this.removeFriends();
                if(tirage(20)==0)this.showCharts(tirage(3));
                if(tirage(15)==0)this.shareBets(tirage(emails));
                //if(tirage(60)==0){this.quitEvent();break;}
                if(tirage(30)==0){this.addPhoto(tirage(photos));break;}

            } catch(StaleElementReferenceException e){
                Main.lg(e.getMessage());
            } catch (ClassCastException e){
                Main.lg(e.getMessage());
            }

            this.sleep(Double.valueOf(tirage(waitDelay))+2);
        }
    }


    private void changeUserPicture() {
    }

    private void openWidget() {
        
    }

    private boolean selectMessage(){
        if(web.getCurrentUrl().indexOf("/tabs/")==-1)return(false);
        if(!tab(2))return false;
        return(click(getElts("tweettext")));
    }

    private String votePhoto() {
        Main.lg("VotePhoto:");
        if(selectMessage()){
            String btn="btnLike";
            if(tirage(1)==0)btn="btnDislike";
            click(getElts(btn));
        }
        return "";
    }

    private String removeMessage() {
        Main.lg("removeMessage");
        if(selectMessage())
            click("btnDelMessage");
        return "";
    }

    private String rotatePhoto() {
        Main.lg("rotatePhoto");
        if(selectMessage())
            click(tirage("btnRotateLeft,btnRotateRight"));
        return "";
    }

    private String postMessage(String message) {
        Main.lg("PostMessage:"+message);
        if(web.getCurrentUrl().indexOf("/tabs/")==-1)return("");
        if(!tab(2))return "";
        send(message,getElt("txtMessage"));
        click("btnSendMessage");
        sleep(1.0);
        return "";
    }

    private String addPhoto(String tirage) throws SearchException {
        Main.lg("AddPhoto:" + tirage);
        if(web.getCurrentUrl().indexOf("/tabs/")==-1)return("");
        if(!tab(2))return "";
        send(tirage,getElt("txtMessage"));
        click("btnSendMessage");
        sleep(1.0);
        return "";
    }

    private boolean isClosed() {
        if(web.getCurrentUrl().indexOf("/closedEvent?")>0)return true;
        return false;
    }

    public void addBetPattern(String bet) throws SearchException {
            String[] opts=bet.split(";");
            Integer size=opts.length;
            String s=opts[2];
            for(int i=3;i<size-2;i++)s+=";"+opts[i];
            addBetPattern(opts[0], opts[1], s, Integer.parseInt(opts[size - 2]), opts[size - 1]);
    }

    public void addTag(String tags) {
        String[] tg=tags.split(" ");
        if(!tab(6))return;
        send(tg[tirage(tg.length-1)],getElt("txtAddTag"));
        valide("txtAddTag");
    }

    public void sendInvite(String email){
        sleep(1.0);
        List<WebElement> elts=getElts("txtEmail","input");
        if(elts.size()>0){
            send(email,elts.get(0));
            click("btnSend");
            sleep(2.0);
        }
    }

    public void shareInvite(String email){
        if(!tab(5))return;
        sendInvite(email);
    }

    private void addBet(List<String> bets) {
        String bet=bets.get(tirage(bets.size()-1));
        String[] opts=bet.split(";");
        Integer size=opts.length;
        String s=opts[2];
        for(int i=3;i<size-2;i++)s+=";"+opts[i];
        addBet(opts[0], opts[1]+" "+tirage(10000), s, Integer.parseInt(opts[size - 2]), Integer.parseInt(opts[size - 1]));
    }

    private void addLoterie(List<String> loteries) {
        String[] vals = tirage(loteries).split(",");
        Main.lg("#addLoterie:"+vals[0]);
        if(!tab(3))return;
        scrollTop();
        if(click(getElt("btnNewLoterie"))) {
            send(vals[0], getElt("txtTitle"));
            send(vals[1], getElt("txtDescription"));
            send(vals[2], getElt("txtStart"));
            send(vals[3], getElt("txtDuration"));
            send(vals[4], getElt("txtMinCredit"));
            send(vals[5], getElt("txtMaxCredit"));
            click("btnSave");
        }
    }


    private void addSurvey(List<String> surveys) {
        String survey=surveys.get(tirage(surveys.size()-1));
        String[] opts=survey.split(";");
        Integer size=opts.length;
        String s=opts[2];
        for(int i=3;i<size-2;i++)s+=";"+opts[i];
        addSurvey(opts[0], opts[1]+" "+tirage(10000), s, Integer.parseInt(opts[size - 2]), Integer.parseInt(opts[size - 1]));
    }

    public void testPari() throws SearchException,StaleElementReferenceException {
        addBet(bets);
        misebet(tirage(40));
        shareBets("sophie.dudule@gmail.com");
        sleep(20.0);
        validateBets();
    }

    public void testMusic() throws SearchException {
        addSong("pixies", tirage(5));
        addSong("nirvana",tirage(5));
        addSong("sonic youth",tirage(5));
        //playMusic("hhoareau@gmail.com","hh4271!!");
    }

    public void createEvent(String evt) {
        String[] params=evt.split(";");
        makeEvent(params[0],params[1],params[2],null,"",null);
    }

    public void createEvents(List<String> events) {
        for(String evt:events){
            String[] params=evt.split(";");
            //new Date(params[3]).getTime()
            makeEvent(params[0],params[1],params[2],null,"",null);
        }
    }

    public void testMail() throws SearchException {
        addBet(this.bets);
        shareBets("sophie.dudule@gmail.com");
        misebet(tirage(40));
        shareBets("sophie.dudule@gmail.com");
        addSurvey(surveys);
        shareBets("sophie.dudule@gmail.com",1);

        shareInvite("sophie.dudule@gmail.com");
        this.sharePlaylist("sophie.dudule@gmail.com");
    }

    private void sharePlaylist(String email) {
        if(!tab(1))return;
        if(click("btnSharePlaylist")) sendInvite(email);
    }

    public void zoom(int i) {
        //if (web instanceof JavascriptExecutor)
        //    ((JavascriptExecutor) web).executeScript("document.body.style.zoom='"+i+"%';");
    }

    public boolean inEvent() {
        return(web.getCurrentUrl().indexOf("/tabs/")>0);
    }

    public void scenario1(String user,String password) throws SearchException {
        openClient("paul.dudule@gmail.com","hh4271!!");
        selEvent(null);
        addSong("Cure",6);
        for(int i=0;i<2;i++)addBet("","bet"+tirage(10000),"opt1;opt2;opt3",0,10);

        for(int i=0;i<50;i++){
            this.validateBets();
            sleep(2.0);
        }
    }

    public Boolean tab(Integer index){
        sleep(4.0);
        if(index==0)index=1;
        List<WebElement> as=getElts("class","tab-item","a");
        if(as.size()>=index){
            click(as.get(index-1));
            return true;
        }
        return false;
    }

    public void scenario_tennis() throws SearchException {
        //raz();
        do{
            openClient("hhoareau@gmail.com","hh4271!!");
            quitEvent();
        }
        while(!makeEvent("Tennis party@Bercy","8eme de finale","8 rue de Bercy, paris","03/11/16 19:00",null,null));

        for(int i=0;i<10;i++)if(selEvent(null))break;
        addSurvey(surveys.subList(2, 2));
        addBet(bets.subList(2,3));
        addBet(bets.subList(3,4));
        addBet(bets.subList(4,5));

        //shareInvite("1phdillmann@gmail.com");
        //shareInvite("1charlotte.nenner@gmail.com");
        //shareInvite("1m.mathonnat@wanadoo.fr");
        shareInvite("1pmisard@hotmail.com");
        quitEvent();
        logOff();

        openClient("paul.dudule@gmail.com","hh4271!!");
        selEvent(null);
        misebet(10);
        misebet(5);
        voteSurvey();
        quitEvent();
        logOff();
    }
}